package _tests

import (
	"context"
	"time"

	mock_transaction "base/internal/mocks/repository/transaction"
	"go.uber.org/mock/gomock"
)

type DefaultServiceTests struct{}

func (d DefaultServiceTests) DefaultTimeServiceLocale() *time.Location {
	return time.FixedZone("currentLocale", int(3)*3600)
}

func (d DefaultServiceTests) GetTx(ctrl *gomock.Controller) *mock_transaction.MockTx {
	tx := mock_transaction.NewMockTx(ctrl)
	return tx
}

func (d DefaultServiceTests) MockTx(
	ctrl *gomock.Controller, mtr *mock_transaction.MockRepository, ctx context.Context,
) *mock_transaction.MockTx {
	tx := mock_transaction.NewMockTx(ctrl)
	mtr.EXPECT().BeginTx(ctx).Return(tx, nil)
	return tx
}

func (d DefaultServiceTests) MockTxRollback(
	ctrl *gomock.Controller, mtr *mock_transaction.MockRepository, ctx context.Context,
) *mock_transaction.MockTx {
	tx := mock_transaction.NewMockTx(ctrl)
	mtr.EXPECT().BeginTx(ctx).Return(tx, nil)
	tx.EXPECT().Rollback()
	return tx
}

func (d DefaultServiceTests) MockTxCommit(
	ctrl *gomock.Controller, mtr *mock_transaction.MockRepository, ctx context.Context,
) *mock_transaction.MockTx {
	tx := mock_transaction.NewMockTx(ctrl)
	mtr.EXPECT().BeginTx(ctx).Return(tx, nil)
	tx.EXPECT().Commit().Return(nil)
	tx.EXPECT().Rollback()
	return tx
}
