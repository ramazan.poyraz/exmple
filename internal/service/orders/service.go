package orders

import (
	"context"
	"database/sql"

	"base/internal/adapter/orders_queue"
	"base/internal/domain"
	"base/internal/errors"
	"base/internal/logger"
	"base/internal/repository/models"
	"base/internal/repository/orders"
	"base/internal/repository/transaction"
)

//go:generate mockgen -source service.go -destination ../../mocks/service/orders/service.go

type (
	Service interface {
		Get(ctx context.Context, id int64) (domain.Order, *errors.Error)
		Create(ctx context.Context, order domain.Order) (domain.Order, *errors.Error)
		CreateQueue(ctx context.Context, order domain.Order) *errors.Error
	}

	service struct {
		log logger.Logger

		orderQueueAdapter orders_queue.Adapter

		txRepo     transaction.Repository
		ordersRepo orders.Repository
	}
)

func NewService(
	log logger.Logger,

	orderQueueAdapter orders_queue.Adapter,

	transactionsRepo transaction.Repository,
	ordersRepo orders.Repository,
) Service {
	return &service{
		log:               log,
		orderQueueAdapter: orderQueueAdapter,
		txRepo:            transactionsRepo,
		ordersRepo:        ordersRepo,
	}
}

func (s *service) Get(ctx context.Context, id int64) (o domain.Order, e *errors.Error) {
	tx, err := s.txRepo.BeginTx(ctx)
	if err != nil {
		return o, s.log.ErrorTx(err)
	}
	defer tx.Rollback()

	data, err := s.ordersRepo.GetById(ctx, tx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return o, errors.OrderNotFound(id)
		}
		return o, errors.DatabaseError(err)
	}
	o = domain.Order{
		Id:    data.Id,
		Title: data.Title,
	}

	if err := tx.Commit(); err != nil {
		return o, s.log.ErrorTx(err)
	}

	return o, nil
}

func (s *service) Create(ctx context.Context, order domain.Order) (o domain.Order, e *errors.Error) {
	data, err := s.ordersRepo.CreateNonTx(ctx, models.Order{Title: order.Title})
	if err != nil {
		return o, errors.DatabaseError(err)
	}
	o = domain.Order{
		Id:    data.Id,
		Title: data.Title,
	}

	return o, nil
}

func (s *service) CreateQueue(ctx context.Context, order domain.Order) *errors.Error {
	if err := s.orderQueueAdapter.CreateOrder(ctx, order); err != nil {
		return errors.WD(errors.InternalError, err)
	}

	return nil
}
