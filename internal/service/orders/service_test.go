package orders

import (
	"context"
	"testing"

	"base/internal/domain"
	"base/internal/logger"
	mock_orders_queue "base/internal/mocks/adapter/orders_queue"
	mock_orders "base/internal/mocks/repository/admin"
	mock_transaction "base/internal/mocks/repository/transaction"
	"base/internal/repository/models"
	"base/internal/service/_tests"
	"github.com/stretchr/testify/suite"
	"go.uber.org/mock/gomock"
)

type Suite struct {
	suite.Suite
	ctrl *gomock.Controller
	_tests.DefaultServiceTests

	service Service

	mockOrdersQueueAdapter *mock_orders_queue.MockAdapter

	mockTxRepo     *mock_transaction.MockRepository
	mockOrdersRepo *mock_orders.MockRepository
}

func TestSuite(t *testing.T) {
	suite.Run(t, new(Suite))
}

func (s *Suite) SetupSuite() {
	s.ctrl = gomock.NewController(s.T())

	s.mockOrdersQueueAdapter = mock_orders_queue.NewMockAdapter(s.ctrl)

	s.mockTxRepo = mock_transaction.NewMockRepository(s.ctrl)
	s.mockOrdersRepo = mock_orders.NewMockRepository(s.ctrl)

	s.service = NewService(logger.NewMockLogger(), s.mockOrdersQueueAdapter, s.mockTxRepo, s.mockOrdersRepo)
}

func (s *Suite) TearDownSuite() {
	s.ctrl.Finish()
}

func (s *Suite) TestGet() {
	ctx := context.Background()
	tx := s.MockTxCommit(s.ctrl, s.mockTxRepo, ctx)
	var orderId int64 = 1984

	modelsOrder := models.Order{Id: orderId, Title: "title"}
	s.mockOrdersRepo.EXPECT().GetById(ctx, tx, orderId).Return(modelsOrder, nil)

	order, e := s.service.Get(ctx, orderId)
	s.Require().Nil(e)

	_order := domain.Order{
		Id:    modelsOrder.Id,
		Title: modelsOrder.Title,
	}
	s.Equal(_order, order)
}

func (s *Suite) TestCreate() {
	ctx := context.Background()
	tx := s.MockTxCommit(s.ctrl, s.mockTxRepo, ctx)

	createExp := models.Order{Title: "title"}
	orderWithId := createExp
	orderWithId.Id = 1984
	s.mockOrdersRepo.EXPECT().Create(ctx, tx, createExp).Return(orderWithId, nil)

	createOrder := domain.Order{Title: "title"}
	order, e := s.service.Create(ctx, createOrder)
	s.Require().Nil(e)

	_order := domain.Order{
		Id:    orderWithId.Id,
		Title: order.Title,
	}
	s.Equal(_order, order)
}
