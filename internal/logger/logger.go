package logger

import (
	"fmt"
	"os"

	"base/internal/errors"
	"go.uber.org/zap/zapcore"

	"go.uber.org/zap"
)

type (
	Logger interface {
		Zap() *zap.Logger

		Named(name string) Logger

		Info(msg string, fields ...zap.Field)
		Warn(text string, fields ...zap.Field)
		Panic(text string, fields ...zap.Field)

		Error(err error, reason string) error
		ErrorTx(err error) *errors.Error
		ErrorRepo(err, reason error, query string) error
	}

	logger struct {
		log *zap.Logger
	}
)

func NewLogger() Logger {
	encoderCfg := zap.NewProductionEncoderConfig()
	encoderCfg.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderCfg.LevelKey = "lvl"

	core := &ZapCoreCustom{
		Core: zapcore.NewCore(
			zapcore.NewJSONEncoder(encoderCfg),
			zapcore.Lock(os.Stdout),
			zap.NewAtomicLevel(),
		),
	}
	l := zap.New(core, zap.AddCaller())

	return &logger{
		log: l,
	}
}

func NewMockLogger() Logger {
	return &logger{log: zap.NewNop()}
}

func (l *logger) Zap() *zap.Logger {
	return l.log
}

func (l *logger) Named(name string) Logger {
	return &logger{
		log: l.log.Named(name),
	}
}

func (l *logger) Panic(text string, fields ...zap.Field) {
	l.log.Panic(text, fields...)
}

func (l *logger) Warn(text string, fields ...zap.Field) {
	l.log.Warn(text, fields...)
}

func (l *logger) Info(text string, fields ...zap.Field) {
	l.log.Info(text, fields...)
}

func (l *logger) Error(err error, reason string) error {
	if reason == "nil" {
		reason = err.Error()
	}

	l.log.Error(formError(err, reason))
	return err
}

func (l *logger) ErrorRepo(err, reason error, query string) error {
	if reason == nil {
		reason = err
	}

	e, zapMethodField, zapReasonField, zapStackField, zapCallerField, zapFullStackField := formError(err, reason.Error())
	errorMsg := fmt.Sprintf("%s \n %s", e, query)
	l.log.Error(errorMsg, zapMethodField, zapReasonField, zapStackField, zapCallerField, zapFullStackField)
	return err
}

func (l *logger) ErrorTx(err error) *errors.Error {
	l.log.Error(formError(err, errors.TxError.Reason))
	return errors.DatabaseError(err)
}
