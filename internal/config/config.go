package config

import (
	"fmt"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v3"
)

type (
	Config struct {
		Server    Server   `yaml:"server"`
		SentryDSN string   `yaml:"sentry_dsn"`
		Postgres  Postgres `yaml:"postgres"`
		RabbitMQ  RabbitMQ `yaml:"rabbitmq"`
	}

	Server struct {
		Port int `yaml:"port"`
	}

	RabbitMQ struct {
		OrdersExchangeName string `yaml:"orders_exchange_name"`
		OrdersBindingKey   string `yaml:"orders_binding_key"`
		OrdersWorkerCount  int    `yaml:"orders_worker_count"`
		OrdersQueueName    string `yaml:"orders_queue_name"`
	}

	Postgres struct {
		Host     string `yaml:"host"`
		Port     string `yaml:"port"`
		Database string `yaml:"database"`
		Username string `yaml:"username"`
		Password string `yaml:"password"`
		CertLoc  string `yaml:"cert_loc"`
	}
)

func New(path, env string) (cfg *Config, err error) {
	f, err := os.Open(getPath(path, env))
	if err != nil {
		return nil, err
	}
	defer f.Close()

	cfg = new(Config)
	var decoder = yaml.NewDecoder(f)
	if err = decoder.Decode(&cfg); err != nil {
		return nil, err
	}

	return cfg, nil
}

func getPath(path, env string) (filePath string) {
	if env == "" {
		env = "development"
	}

	var name = fmt.Sprintf("app.%s.yaml", env)
	return filepath.Join(path, name)
}
