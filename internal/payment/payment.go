package payment

type Payment struct {
	Amount   float64
	Currency string
	Receiver string
}

type PaymentProvider interface {
	Init(config map[string]string) error
	Auth(payment Payment) (string, error)
	Capture(paymentID string, amount float64) error
	Refund(paymentID string, amount float64) error
}
