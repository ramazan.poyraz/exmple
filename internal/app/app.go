package app

import (
	"base/internal/dependencies"
)

type (
	Application interface {
		Run()
		RunRabbitmq()
	}

	application struct {
		deps dependencies.Dependencies
	}
)

func NewApplication(cfgPath, cfgEnv string) Application {
	deps, err := dependencies.NewDependencies(cfgPath, cfgEnv)
	if err != nil {
		panic(err)
	}
	return &application{
		deps: deps,
	}
}

func (app *application) Run() {
	appServer := app.deps.AppServer()
	appServer.Start()

	app.deps.WaitForInterrupt()

	app.deps.Close()
}

func (app *application) RunRabbitmq() {
	rabbitServer := app.deps.RabbitmqServer()
	rabbitServer.Start()

	app.deps.WaitForInterrupt()

	app.deps.Close()
}
