package rabbitmq

import (
	"context"
	"encoding/json"
	"sync"
	"time"

	"base/internal/config"
	"base/internal/domain"
	"base/internal/errors"
	"base/internal/handler/types"
	"base/internal/logger"
	amqp "github.com/rabbitmq/amqp091-go"
	"go.uber.org/zap"
)

type (
	ordersCreateHandler struct {
		log logger.Logger
		cfg config.RabbitMQ

		service ordersCrateInterface
	}

	ordersCrateInterface interface {
		Create(ctx context.Context, order domain.Order) (domain.Order, *errors.Error)
	}
)

func NewOrdersCreateHandler(
	log logger.Logger,
	cfg config.RabbitMQ,
	service ordersCrateInterface,
) Handler {
	return &ordersCreateHandler{
		log:     log,
		cfg:     cfg,
		service: service,
	}
}

func (h *ordersCreateHandler) New(wg *sync.WaitGroup, connection *amqp.Connection) InstanceRunner {
	ch, err := connection.Channel()
	if err != nil {
		h.log.Panic("Failed to open channel", zap.Error(err))
	}

	consumer, err := NewConsumer(h.log, ch, h.cfg.OrdersExchangeName, h.cfg.OrdersQueueName, h.cfg.OrdersBindingKey)
	if err != nil {
		h.log.Panic("Failed to create consumer", zap.Error(err))
	}

	if err := consumer.NewQueue(); err != nil {
		h.log.Panic("Failed to create new queue", zap.Error(err))
	}

	worker := &RabbitMQOrdersCreateWorker{service: h.service}
	return NewController(
		h.log,
		connection,
		wg,
		consumer,
		worker,
		h.cfg.OrdersQueueName,
	)
}

type RabbitMQOrdersCreateWorker struct {
	log logger.Logger

	service ordersCrateInterface

	qid string
}

func (w *RabbitMQOrdersCreateWorker) SetQid(qid string) {
	w.qid = qid
}

func (w *RabbitMQOrdersCreateWorker) Validate(ctx context.Context, body []byte) (any, WorkerResponseAction, error) {
	var message types.CreateOrder
	if err := json.Unmarshal(body, &message); err != nil {
		return nil, Nack, w.log.Error(err, "failed to unmarshal message")
	}

	if message.Title == "" {
		return nil, Nack, w.log.Error(nil, "title cannot be empty")
	}

	return message, None, nil
}

func (w *RabbitMQOrdersCreateWorker) Execute(ctx context.Context, _message any, rawMessage []byte) (WorkerResponseAction, error) {
	message, ok := _message.(types.CreateOrder)
	if !ok {
		return Nack, errors.New("failed to asser message (RabbitMQOrdersCreateWorker) to *types.CreateOrder")
	}

	_order := domain.Order{
		Title: message.Title,
	}
	_, err := w.service.Create(ctx, _order)
	if err != nil {
		return Nack, err
	}

	return Ack, nil
}

func (w *RabbitMQOrdersCreateWorker) NackInterval() time.Duration {
	return 0
}
