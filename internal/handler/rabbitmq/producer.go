package rabbitmq

import (
	"context"
	"fmt"

	"base/internal/logger"
	amqp "github.com/rabbitmq/amqp091-go"
)

type Producer struct {
	log logger.Logger

	channel  *amqp.Channel
	confirms chan amqp.Confirmation

	exchangeName, queueName, bindingKey string
}

func NewProducer(
	log logger.Logger,
	channel *amqp.Channel,
	confirms chan amqp.Confirmation,
	exchangeName, queueName, bindingKey string,
) (*Producer, error) {
	return &Producer{
		log:      log,
		channel:  channel,
		confirms: confirms,

		exchangeName: exchangeName,
		queueName:    queueName,
		bindingKey:   bindingKey,
	}, nil
}

func (c *Producer) NewQueue() (err error) {
	var (
		args = make(amqp.Table)
		kind = "direct"
	)

	if err = c.channel.ExchangeDeclare(c.exchangeName, kind, true, false, false, false, args); err != nil {
		return c.log.Error(err, fmt.Sprintf("failed to declare exchange %s", c.exchangeName))
	}

	_, err = c.channel.QueueDeclare(
		c.queueName,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return fmt.Errorf("consumer Declare: %s", err)
	}

	if err = c.channel.QueueBind(c.queueName, c.bindingKey, c.exchangeName, false, nil); err != nil {
		return err
	}

	return nil
}

func (c *Producer) PublishMessage(ctx context.Context, body []byte) error {
	err := c.channel.PublishWithContext(
		ctx,
		c.exchangeName, // exchange
		c.bindingKey,   // routing key
		false,          // mandatory
		false,          // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        body,
		})
	if err != nil {
		return c.log.Error(err, "failed to publish message")
	}

	if c.confirms != nil {
		<-c.confirms
	}

	return nil
}
