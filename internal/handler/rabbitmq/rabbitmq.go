package rabbitmq

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/gofrs/uuid"

	"base/internal/logger"
	amqp "github.com/rabbitmq/amqp091-go"
	"go.uber.org/zap"
)

type InstanceRunner interface {
	Run()
	Stop()
}

type Handler interface {
	New(wg *sync.WaitGroup, connection *amqp.Connection) InstanceRunner
}

type Worker interface {
	Validate(ctx context.Context, body []byte) (any, WorkerResponseAction, error)
	Execute(ctx context.Context, message any, rawMessage []byte) (WorkerResponseAction, error)
	NackInterval() time.Duration

	SetQid(qid string)
}

type Controller struct {
	log        logger.Logger
	stopSignal chan struct{}
	conn       *amqp.Connection
	wg         *sync.WaitGroup

	consumer *Consumer
	worker   Worker

	id        string
	queueName string
}

func NewController(
	log logger.Logger,
	conn *amqp.Connection,
	wg *sync.WaitGroup,

	consumer *Consumer,
	worker Worker,

	queueName string,
) *Controller {
	_id, _ := uuid.NewV4()
	id := _id.String()

	worker.SetQid(id)

	return &Controller{
		log:        log,
		stopSignal: make(chan struct{}),
		conn:       conn,
		wg:         wg,

		consumer: consumer,
		worker:   worker,

		id:        id,
		queueName: queueName,
	}
}

func (c *Controller) Run() {
	defer func() {
		c.wg.Done()
	}()
	c.log.Info("run worker", zap.Any("qid", c.id), zap.String("qname", c.queueName))

	messages, err := c.ReceiveMessages()
	if err != nil {
		c.log.Zap().Error("ReceiveMessages", zap.Error(err), zap.Any("qid", c.id), zap.String("qname", c.queueName))
	}

	for {
		select {
		case s := <-c.stopSignal:
			c.log.Info("stopSignal", zap.Any("signal", s), zap.Any("qid", c.id), zap.String("qname", c.queueName))
			return
		case message := <-messages:
			if len(message.Body) == 0 {
				messages, _ = c.ReceiveMessages()
				break
			}
			c.handling(context.Background(), message)
		}
	}
}

func (c *Controller) Stop() {
	c.stopSignal <- struct{}{}
}

func (c *Controller) ReceiveMessages() (messages <-chan amqp.Delivery, err error) {
	for {
		messages, err = c.consumer.GetMessages()
		if err != nil {
			fmt.Println(err)
			c.log.Zap().Error("GetMessages", zap.Error(err), zap.String("name", c.consumer.queueName))
			time.Sleep(time.Second * 5)
			continue
		}
		break
	}
	return messages, err
}

func (c *Controller) handling(ctx context.Context, message amqp.Delivery) {
	_message, action, err := c.worker.Validate(ctx, message.Body)
	if err != nil {
		return
	}
	if action == Ack {
		_ = c.ack(message)
		return
	} else if action == Nack {
		_ = c.nack(ctx, message)
		return
	}

	action, err = c.worker.Execute(ctx, _message, message.Body)
	if err != nil {
		_ = c.nack(ctx, message)
		return
	}
	if action == Ack {
		_ = c.ack(message)
		return
	} else if action == Nack {
		return
	}
}

func (c *Controller) nack(ctx context.Context, message amqp.Delivery) error {
	time.Sleep(c.worker.NackInterval())

	if err := message.Nack(false, true); err != nil {
		c.log.Zap().Error("message.Nack", zap.Error(err))
		return c.nack(ctx, message)
	}
	return nil
}

func (c *Controller) ack(message amqp.Delivery) error {
	return message.Ack(false)
}
