package rabbitmq

import (
	"fmt"

	"base/internal/logger"
	amqp "github.com/rabbitmq/amqp091-go"
)

type Consumer struct {
	log logger.Logger

	channel                             *amqp.Channel
	exchangeName, queueName, bindingKey string
}

func NewConsumer(log logger.Logger, channel *amqp.Channel, exchangeName, queueName, bindingKey string) (*Consumer, error) {
	return &Consumer{
		log: log,

		channel:      channel,
		exchangeName: exchangeName,
		queueName:    queueName,
		bindingKey:   bindingKey,
	}, nil
}

func (c *Consumer) NewQueue() (err error) {
	var (
		args = make(amqp.Table)
		kind = "direct"
	)

	if err = c.channel.Qos(
		1,
		0,
		false,
	); err != nil {
		return c.log.Error(err, "failed to set QoS")
	}

	if err = c.channel.ExchangeDeclare(c.exchangeName, kind, true, false, false, false, args); err != nil {
		return c.log.Error(err, fmt.Sprintf("failed to declare exchange %s", c.exchangeName))
	}

	_, err = c.channel.QueueDeclare(
		c.queueName,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return fmt.Errorf("consumer Declare: %s", err)
	}

	if err = c.channel.QueueBind(c.queueName, c.bindingKey, c.exchangeName, false, nil); err != nil {
		return err
	}

	return nil
}

func (c *Consumer) GetMessages() (<-chan amqp.Delivery, error) {
	var message <-chan amqp.Delivery
	messages, err := c.channel.Consume(
		c.queueName,
		"",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return message, err
	}

	return messages, nil
}
