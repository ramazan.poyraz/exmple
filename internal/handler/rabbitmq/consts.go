package rabbitmq

type WorkerResponseAction int

const (
	None = WorkerResponseAction(iota)
	Ack
	Nack
)
