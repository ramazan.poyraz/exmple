package http

import (
	"encoding/json"
	"net/http"

	"base/internal/errors"
	"base/internal/generated/models"
)

func encodeError(w http.ResponseWriter, err *errors.Error) {
	_ = json.NewEncoder(w).Encode(models.Response{
		Err: &models.Error{
			ErrCode: err.Code,
			ErrText: err.Error(),
		},
	})
}

func encodeResponse(w http.ResponseWriter, response interface{}) {
	w.WriteHeader(http.StatusOK)

	e, ok := response.(*errors.Error)
	if ok {
		encodeError(w, e)
		return
	}

	if err := json.NewEncoder(w).Encode(response); err != nil {
		encodeError(w, errors.WD(errors.InternalError, err))
		return
	}
}
