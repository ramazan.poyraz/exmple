package http

import (
	"encoding/json"
	"net/http"
	"strconv"

	"base/internal/domain"
	"base/internal/errors"
	"base/internal/generated/models"
	"base/internal/handler/middleware"
	"base/internal/service/orders"
	"github.com/gorilla/mux"
)

type (
	ordersHandler struct {
		service orders.Service

		middleware middleware.Middleware
	}
)

func NewOrdersHandler(
	service orders.Service,
	middleware middleware.Middleware,
) Handler {
	return &ordersHandler{
		service:    service,
		middleware: middleware,
	}
}

func (h *ordersHandler) FillMuxHandlers(router *mux.Router) {
	r := router.PathPrefix("/orders").Subrouter()

	r.HandleFunc("/{order_id}", h.get).Methods(http.MethodGet)
	r.HandleFunc("", h.post).Methods(http.MethodPost)
	r.HandleFunc("/queue", h.postQueue).Methods(http.MethodPost)

	r.Use(
		h.middleware.LoggerMiddleware,
	)
}
func (h *ordersHandler) Shutdown() {}

func (h *ordersHandler) get(w http.ResponseWriter, r *http.Request) {
	var vars = mux.Vars(r)
	orderIdString, ok := vars["order_id"]
	if !ok {
		encodeError(w, errors.WDString(errors.NoQueryParameterError, "order_id is required"))
		return
	}
	orderId, err := strconv.ParseInt(orderIdString, 10, 64)
	if err != nil {
		encodeError(w, errors.WD(errors.InternalError, err))
		return
	}

	order, e := h.service.Get(r.Context(), orderId)
	if e != nil {
		encodeError(w, e)
		return
	}

	resp := models.OrderResponse{
		Order: models.Order{
			Id:    order.Id,
			Title: order.Title,
		},
	}
	encodeResponse(w, resp)
}

func (h *ordersHandler) post(w http.ResponseWriter, r *http.Request) {
	createOrderData := &models.OrderToCreate{}
	if err := json.NewDecoder(r.Body).Decode(&createOrderData); err != nil {
		encodeError(w, errors.WD(errors.ParseError, err))
		return
	}

	// validation block
	if createOrderData.Title == "" {
		encodeError(w, errors.WDString(errors.ValidationError, "title is required"))
		return
	}

	dataToCreate := domain.Order{
		Title: createOrderData.Title,
	}
	order, e := h.service.Create(r.Context(), dataToCreate)
	if e != nil {
		encodeError(w, e)
		return
	}

	resp := models.OrderResponse{
		StatusCode: http.StatusCreated,
		Err:        nil,
		Order: models.Order{
			Id:    order.Id,
			Title: order.Title,
		},
	}
	encodeResponse(w, resp)
}

func (h *ordersHandler) postQueue(w http.ResponseWriter, r *http.Request) {
	createOrderData := &models.OrderToCreate{}
	if err := json.NewDecoder(r.Body).Decode(&createOrderData); err != nil {
		encodeError(w, errors.WD(errors.ParseError, err))
		return
	}

	// validation block
	if createOrderData.Title == "" {
		encodeError(w, errors.WDString(errors.ValidationError, "title is required"))
		return
	}

	dataToCreate := domain.Order{
		Title: createOrderData.Title,
	}
	e := h.service.CreateQueue(r.Context(), dataToCreate)
	if e != nil {
		encodeError(w, e)
		return
	}

	resp := models.Response{
		StatusCode: http.StatusOK,
		Err:        nil,
	}
	encodeResponse(w, resp)
}
