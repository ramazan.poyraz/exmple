package http

import (
	"github.com/gorilla/mux"
)

type Handler interface {
	FillMuxHandlers(router *mux.Router)
}
