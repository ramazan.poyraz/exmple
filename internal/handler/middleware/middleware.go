package middleware

import (
	"net/http"

	"base/internal/logger"
)

type (
	Middleware interface {
		LoggerMiddleware(next http.Handler) http.Handler
	}

	middleware struct {
		log logger.Logger
	}
)

func NewMiddleware(
	log logger.Logger,
) Middleware {
	return &middleware{
		log: log,
	}
}

func (m *middleware) LoggerMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r)
	})
}
