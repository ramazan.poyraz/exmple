package orders_queue

import (
	"context"
	"encoding/json"

	"base/internal/config"
	"base/internal/connections"
	"base/internal/domain"
	"base/internal/errors"
	"base/internal/handler/rabbitmq"
	"base/internal/logger"
)

//go:generate mockgen -source adapter.go -destination ../../mocks/adapter/orders_queue/adapter.go

type (
	Adapter interface {
		CreateOrder(ctx context.Context, order domain.Order) error
	}

	adapter struct {
		log logger.Logger

		conn *connections.Rabbitmq

		createOrderProducerWithConfirms *rabbitmq.Producer
		createOrderProducer             *rabbitmq.Producer
	}
)

func NewAdapter(
	log logger.Logger,
	conn *connections.Rabbitmq,
	cfg config.RabbitMQ,
) (Adapter, *errors.Error) {
	createOrdersProducerWithConfirms, err := newProducerWithConfirms(log, conn, cfg.OrdersExchangeName, cfg.OrdersQueueName, cfg.OrdersBindingKey)
	if err != nil {
		return nil, err
	}

	createOrdersProducer, err := newProducer(log, conn, cfg.OrdersExchangeName, cfg.OrdersQueueName, cfg.OrdersBindingKey)
	if err != nil {
		return nil, err
	}

	return &adapter{
		log:                             log,
		conn:                            conn,
		createOrderProducerWithConfirms: createOrdersProducerWithConfirms,
		createOrderProducer:             createOrdersProducer,
	}, nil
}

func (a *adapter) CreateOrder(ctx context.Context, order domain.Order) error {
	body, err := json.Marshal(&order)
	if err != nil {
		return a.log.Error(err, "create marshal order fail")
	}

	if err := a.createOrderProducer.PublishMessage(ctx, body); err != nil {
		return errors.WDF(errors.InternalError, "publish message fail: %s", err)
	}

	return nil
}
