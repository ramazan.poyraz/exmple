package orders_queue

import (
	"base/internal/connections"
	"base/internal/errors"
	"base/internal/handler/rabbitmq"
	"base/internal/logger"
	amqp "github.com/rabbitmq/amqp091-go"
)

func newProducer(
	log logger.Logger,
	conn *connections.Rabbitmq,
	exchangeName string,
	queueName string,
	bindingKey string,
) (*rabbitmq.Producer, *errors.Error) {
	ch, err := conn.Conn.Channel()
	if err != nil {
		return nil, errors.WDF(errors.InternalError, "create order channel fail: %s", err)
	}

	producer, err := rabbitmq.NewProducer(log, ch, nil, exchangeName, queueName, bindingKey)
	if err != nil {
		return nil, errors.WDF(errors.InternalError, "create order producer fail: %s", err)
	}

	return producer, nil
}

func newProducerWithConfirms(
	log logger.Logger,
	conn *connections.Rabbitmq,
	exchangeName string,
	queueName string,
	bindingKey string,
) (*rabbitmq.Producer, *errors.Error) {
	ch, err := conn.Conn.Channel()
	if err != nil {
		return nil, errors.WDF(errors.InternalError, "create order channel fail: %s", err)
	}
	err = ch.Confirm(false)
	if err != nil {
		return nil, errors.WDF(errors.InternalError, "create order channel fail: %s", err)
	}
	confirms := ch.NotifyPublish(make(chan amqp.Confirmation, 1))

	producer, err := rabbitmq.NewProducer(log, ch, confirms, exchangeName, queueName, bindingKey)
	if err != nil {
		return nil, errors.WDF(errors.InternalError, "create order producer fail: %s", err)
	}

	return producer, nil
}
