package psql_sqlx

var tablesToDelete = `
	DELETE FROM orders;

	ALTER SEQUENCE orders_id_seq RESTART WITH 1;
`
