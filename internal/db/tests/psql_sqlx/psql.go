package psql_sqlx

import (
	"context"
	"time"

	"base/internal/config"
	"base/internal/db"
	"base/internal/logger"
	"base/internal/repository/transaction"
	"github.com/jmoiron/sqlx"
	"github.com/pressly/goose/v3"
	"github.com/stretchr/testify/suite"
)

type PsqlSqlxParallelSuite struct {
	suite.Suite

	transactionsRepo transaction.Repository

	ctx context.Context
	cfg config.Postgres
	db  *sqlx.DB

	TestPsqlClient *db.PostgresClient

	testTx transaction.Tx
	time   time.Time
}

func (s *PsqlSqlxParallelSuite) SetupSuite() {
	cfg, err := config.New("../../../configs", "localtest")
	s.Require().NoError(err)
	s.cfg = cfg.Postgres

	testClient, err := db.NewPostgresSqlxClient(s.cfg)
	s.Require().NoError(err)
	s.TestPsqlClient = testClient
	s.db = s.TestPsqlClient.DBSqlx

	s.transactionsRepo = transaction.NewPgxRepository(s.TestPsqlClient)

	goose.SetLogger(&logger.EmptyLogger{})
}

func (s *PsqlSqlxParallelSuite) SetupTest() {
	s.ctx = context.Background()

	tx, err := s.transactionsRepo.BeginTx(s.ctx)
	s.Require().NoError(err)
	s.testTx = tx
	s.time = time.Now()

	_, err = s.testTx.Txm().Exec(tablesToDelete)
	s.Require().NoError(err)
}

func (s *PsqlSqlxParallelSuite) TearDownTest() {
	s.testTx.Rollback()
}

func (s *PsqlSqlxParallelSuite) TearDownSuite() {
	s.Require().NoError(s.TestPsqlClient.Close())
}

func (s *PsqlSqlxParallelSuite) Tx() transaction.Tx {
	return s.testTx
}

func (s *PsqlSqlxParallelSuite) CtxTx() (context.Context, transaction.Tx) {
	return context.Background(), s.testTx
}
