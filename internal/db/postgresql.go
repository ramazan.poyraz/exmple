package db

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"os"
	"strings"
	"time"

	"base/internal/config"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/stdlib"
	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/jmoiron/sqlx"
)

type PostgresClient struct {
	DBSqlx *sqlx.DB
}

func NewPostgresSqlxClient(c config.Postgres) (*PostgresClient, error) {
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Bishkek",
		c.Host, c.Username, c.Password, c.Database, c.Port,
	)

	var client *sqlx.DB
	if c.CertLoc != "" && !strings.Contains(dsn, "disable") {
		rootCertPool := x509.NewCertPool()
		pem, err := os.ReadFile(c.CertLoc)
		if err != nil {
			return nil, fmt.Errorf("error reading cert: %w", err)
		}
		rootCertPool.AppendCertsFromPEM(pem)
		connCfg, err := pgx.ParseConfig(dsn)
		if err != nil {
			return nil, fmt.Errorf("error parsing postgres cdn: %w", err)
		}

		connCfg.TLSConfig = &tls.Config{
			RootCAs:            rootCertPool,
			InsecureSkipVerify: true,
		}
		db := stdlib.OpenDB(*connCfg)
		client = sqlx.NewDb(db, "pgx")
	} else {
		var err error
		client, err = sqlx.Connect("pgx", dsn)
		if err != nil {
			return nil, fmt.Errorf("error while connecting to postgres %w", err)
		}
	}
	if err := client.Ping(); err != nil {
		return nil, fmt.Errorf("error while ping to postgres %w", err)
	}

	client.SetMaxOpenConns(100)
	client.SetMaxIdleConns(100)
	client.SetConnMaxLifetime(10 * time.Second)

	return &PostgresClient{
		DBSqlx: client,
	}, nil
}

func (client *PostgresClient) Close() error {
	if client.DBSqlx != nil {
		err := client.DBSqlx.Close()
		if err != nil {
			return err
		}
	}

	return nil
}
