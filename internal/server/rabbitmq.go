package server

import (
	"os"
	"sync"

	"base/internal/config"
	"base/internal/connections"
	"base/internal/handler/rabbitmq"
	"base/internal/logger"
	"go.uber.org/zap"
)

type rabbitServer struct {
	log  logger.Logger
	cfg  config.RabbitMQ
	wg   sync.WaitGroup
	conn *connections.Rabbitmq

	closeCallbacks []func()

	stopSignal chan os.Signal

	ordersHandler rabbitmq.Handler
}

func NewRabbitMQ(
	log logger.Logger,
	cfg config.RabbitMQ,
	conn *connections.Rabbitmq,

	ordersHandler rabbitmq.Handler,
) (Server, error) {
	return &rabbitServer{
		log:  log,
		cfg:  cfg,
		conn: conn,

		closeCallbacks: []func(){},

		ordersHandler: ordersHandler,
	}, nil
}

func (r *rabbitServer) Start() {
	r.log.Zap().Info("Start RabbitMQ server", zap.Int("orders_worker_count", r.cfg.OrdersWorkerCount))
	workers := []WorkerInstance{
		{count: r.cfg.OrdersWorkerCount, instance: r.ordersHandler},
	}

	go func() {
		for _, w := range workers {
			r.wg.Add(w.count)
			for i := 0; i < w.count; i++ {
				instance := w.instance.New(&r.wg, r.conn.Conn)

				go instance.Run()
				r.closeCallbacks = append(r.closeCallbacks, func() {
					instance.Stop()
				})
			}
		}
	}()
}

func (r *rabbitServer) Stop() error {
	for _, cb := range r.closeCallbacks {
		cb()
	}

	r.wg.Wait()

	return nil
}

type WorkerInstance struct {
	count    int
	instance rabbitmq.Handler
}
