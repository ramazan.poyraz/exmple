package server

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"sync"
	"time"

	"base/internal/config"
	internalHttp "base/internal/handler/http"
	"base/internal/logger"
	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

type appServer struct {
	log      logger.Logger
	cfg      config.Server
	wg       sync.WaitGroup
	listener net.Listener
	server   *http.Server

	ordersHandler internalHttp.Handler
}

func NewAppServer(
	log logger.Logger,
	cfg config.Server,

	ordersHandler internalHttp.Handler,
) (Server, error) {
	var err error
	listener, err := net.Listen("tcp", fmt.Sprintf(":%v", cfg.Port))
	if err != nil {
		return nil, fmt.Errorf("cannot listen app port: %w", err)
	}

	router := mux.NewRouter()
	server := &appServer{
		log: log.Named("app_server"),
		cfg: cfg,
		server: &http.Server{
			Addr:    fmt.Sprintf(":%d", cfg.Port),
			Handler: router,
		},
		listener: listener,

		ordersHandler: ordersHandler,
	}
	server.initRoutes(router)
	return server, nil
}

func (s *appServer) initRoutes(router *mux.Router) {
	r := router.PathPrefix("/api").Subrouter()

	s.ordersHandler.FillMuxHandlers(r)
}

func (s *appServer) Start() {
	s.log.Zap().Info("Start app server", zap.Int("port", s.cfg.Port))
	s.wg.Add(1)
	go func() {
		defer s.wg.Done()

		if err := s.server.Serve(s.listener); err != nil && err != http.ErrServerClosed {
			s.log.Zap().Panic("Error while serve app server", zap.Error(err))
		}
	}()
}

func (s *appServer) Stop() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	if err := s.server.Shutdown(ctx); err != nil {
		return err
	}
	s.wg.Wait()
	return nil
}
