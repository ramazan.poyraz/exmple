package dependencies

import (
	"base/internal/connections"
	"base/internal/db"
	"go.uber.org/zap"
)

func (d *dependencies) PostgresClient() *db.PostgresClient {
	if d.psqlClient == nil {
		var err error
		msg := "initialize postgres client"
		if d.psqlClient, err = db.NewPostgresSqlxClient(d.cfg.Postgres); err != nil {
			d.log.Zap().Panic(msg, zap.Error(err))
		}
		d.closeCallbacks = append(d.closeCallbacks, func() {
			if err := d.psqlClient.Close(); err != nil {
				d.log.Zap().Warn(msg, zap.Error(err))
				return
			}
		})
	}
	return d.psqlClient
}

func (d *dependencies) RabbitmqConn() *connections.Rabbitmq {
	if d.rabbitmqConn == nil {
		var err error
		msg := "initialize rabbitmq connection"
		if d.rabbitmqConn, err = connections.NewRabbitMQConn(); err != nil {
			d.log.Zap().Panic(msg, zap.Error(err))
		}
		d.closeCallbacks = append(d.closeCallbacks, func() {
			if err := d.rabbitmqConn.Conn.Close(); err != nil {
				d.log.Zap().Warn(msg, zap.Error(err))
				return
			}
		})
	}
	return d.rabbitmqConn
}
