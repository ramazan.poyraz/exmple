package dependencies

import (
	"base/internal/handler/rabbitmq"
)

func (d *dependencies) OrdersRabbitmqHandler() rabbitmq.Handler {
	if d.ordersRabbitmqHandler == nil {
		d.ordersRabbitmqHandler = rabbitmq.NewOrdersCreateHandler(d.log, d.cfg.RabbitMQ, d.OrdersService())
	}
	return d.ordersRabbitmqHandler
}
