package dependencies

import (
	"base/internal/handler/middleware"
)

func (d *dependencies) HandlerMiddleware() middleware.Middleware {
	if d.handlerMiddleware == nil {
		d.handlerMiddleware = middleware.NewMiddleware(d.log)
	}
	return d.handlerMiddleware
}
