package dependencies

import (
	"base/internal/handler/http"
)

func (d *dependencies) OrdersHttpHandler() http.Handler {
	if d.ordersHttpHandler == nil {
		d.ordersHttpHandler = http.NewOrdersHandler(d.OrdersService(), d.HandlerMiddleware())
	}
	return d.ordersHttpHandler
}
