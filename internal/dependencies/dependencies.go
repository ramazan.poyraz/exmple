package dependencies

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"base/internal/adapter/orders_queue"
	"base/internal/config"
	"base/internal/connections"
	"base/internal/db"
	"base/internal/handler/http"
	"base/internal/handler/middleware"
	"base/internal/handler/rabbitmq"
	"base/internal/logger"
	ordersRepo "base/internal/repository/orders"
	"base/internal/repository/transaction"
	"base/internal/server"
	ordersSvc "base/internal/service/orders"
	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
)

type (
	Dependencies interface {
		WaitForInterrupt()

		AppServer() server.Server
		RabbitmqServer() server.Server

		Close()
	}

	dependencies struct {
		shutdownChannel chan os.Signal
		closeCallbacks  []func()

		cfg *config.Config
		log logger.Logger

		appServer      server.Server
		rabbitmqServer server.Server

		psqlClient   *db.PostgresClient
		rabbitmqConn *connections.Rabbitmq

		handlerMiddleware middleware.Middleware

		ordersHttpHandler     http.Handler
		ordersRabbitmqHandler rabbitmq.Handler

		ordersSvc ordersSvc.Service

		ordersQueue orders_queue.Adapter

		txRepo     transaction.Repository
		ordersRepo ordersRepo.Repository
	}
)

func NewDependencies(cfgPath, cfgEnv string) (Dependencies, error) {
	cfg, err := config.New(cfgPath, cfgEnv)
	if err != nil {
		return nil, err
	}

	if cfg.SentryDSN != "" {
		err = sentry.Init(sentry.ClientOptions{Dsn: cfg.SentryDSN})
		if err != nil {
			return nil, fmt.Errorf("error while init sentry %v", err)
		}
	}

	return &dependencies{
		shutdownChannel: make(chan os.Signal),
		cfg:             cfg,
		log:             logger.NewLogger(),
	}, nil
}

func (d *dependencies) AppServer() server.Server {
	if d.appServer == nil {
		var err error
		if d.appServer, err = server.NewAppServer(
			d.log,
			d.cfg.Server,

			d.OrdersHttpHandler(),
		); err != nil {
			d.log.Zap().Panic("initialize app server", zap.Error(err))
		}

		d.closeCallbacks = append(d.closeCallbacks, func() {
			msg := "close app server"
			if err := d.appServer.Stop(); err != nil {
				d.log.Zap().Warn(msg, zap.Error(err))
				return
			}
			d.log.Zap().Info(msg)
		})
	}
	return d.appServer
}

func (d *dependencies) RabbitmqServer() server.Server {
	if d.rabbitmqServer == nil {
		var err error
		if d.rabbitmqServer, err = server.NewRabbitMQ(
			d.log,
			d.cfg.RabbitMQ,
			d.RabbitmqConn(),

			d.OrdersRabbitmqHandler(),
		); err != nil {
			d.log.Zap().Panic("initialize rebbitmq server", zap.Error(err))
		}

		d.closeCallbacks = append(d.closeCallbacks, func() {
			msg := "close rebbitmq server"
			if err := d.rabbitmqServer.Stop(); err != nil {
				d.log.Zap().Warn(msg, zap.Error(err))
				return
			}
			d.log.Zap().Info(msg)
		})
	}
	return d.rabbitmqServer
}

func (d *dependencies) WaitForInterrupt() {
	signal.Notify(d.shutdownChannel, syscall.SIGINT, syscall.SIGTERM)
	d.log.Zap().Info("Wait for receive interrupt signal")
	<-d.shutdownChannel
	d.log.Zap().Info("Receive interrupt signal")
}

func (d *dependencies) Close() {
	for i := len(d.closeCallbacks) - 1; i >= 0; i-- {
		d.closeCallbacks[i]()
	}
	d.log.Zap().Sync()
}
