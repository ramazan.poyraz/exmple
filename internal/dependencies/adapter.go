package dependencies

import (
	"base/internal/adapter/orders_queue"
	"go.uber.org/zap"
)

func (d *dependencies) OrdersQueueAdapter() orders_queue.Adapter {
	if d.ordersQueue == nil {
		msg := "initialize orders queue adapter"
		queue, err := orders_queue.NewAdapter(d.log, d.RabbitmqConn(), d.cfg.RabbitMQ)
		if err != nil {
			d.log.Zap().Panic(msg, zap.Error(err))
		}
		d.ordersQueue = queue
	}
	return d.ordersQueue
}
