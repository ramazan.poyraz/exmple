package dependencies

import (
	"base/internal/repository/orders"
	"base/internal/repository/transaction"
)

func (d *dependencies) TxRepo() transaction.Repository {
	if d.txRepo == nil {
		d.txRepo = transaction.NewPgxRepository(d.PostgresClient())
	}
	return d.txRepo
}

func (d *dependencies) OrdersRepo() orders.Repository {
	if d.ordersRepo == nil {
		d.ordersRepo = orders.NewPGSqlxRepository(d.log, d.PostgresClient())
	}
	return d.ordersRepo
}
