package dependencies

import (
	"base/internal/service/orders"
)

func (d *dependencies) OrdersService() orders.Service {
	if d.ordersSvc == nil {
		d.ordersSvc = orders.NewService(d.log, d.OrdersQueueAdapter(), d.TxRepo(), d.OrdersRepo())
	}
	return d.ordersSvc
}
