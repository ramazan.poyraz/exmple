package transaction

import (
	"context"

	"github.com/jmoiron/sqlx"
)

//go:generate  mockgen -source interface.go -destination ../../mocks/repository/transaction/interface.go

type (
	Repository interface {
		BeginTx(ctx context.Context) (Tx, error)
	}

	Tx interface {
		Commit() error
		Rollback()
		Txm() *sqlx.Tx
	}
)
