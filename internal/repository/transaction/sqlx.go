package transaction

import (
	"context"

	"base/internal/db"
	"github.com/jmoiron/sqlx"
)

type (
	TxSqlx struct {
		*sqlx.Tx
		ctx context.Context
	}

	repositoryPG struct {
		client *db.PostgresClient
	}
)

func NewPgxRepository(client *db.PostgresClient) Repository {
	return &repositoryPG{
		client: client,
	}
}

func (repo *repositoryPG) BeginTx(ctx context.Context) (Tx, error) {
	tx, err := repo.client.DBSqlx.BeginTxx(ctx, nil)
	if err != nil {
		return nil, err
	}
	return &TxSqlx{
		Tx:  tx,
		ctx: ctx,
	}, nil
}

func (t *TxSqlx) Txm() *sqlx.Tx {
	return t.Tx
}

func (t *TxSqlx) Rollback() {
	_ = t.Tx.Rollback()
}
