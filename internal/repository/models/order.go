package models

type Order struct {
	Id    int64  `db:"id"`
	Title string `db:"title"`
}
