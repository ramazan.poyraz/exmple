package orders

import (
	"testing"

	"base/internal/db/tests/psql_sqlx"
	"base/internal/logger"
	"base/internal/repository/models"
	"github.com/stretchr/testify/suite"
)

type PGSqlxSuite struct {
	psql_sqlx.PsqlSqlxParallelSuite

	repo Repository
}

func TestPGSuite(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping all db related tests for short testing")
	}
	suite.Run(t, new(PGSqlxSuite))
}

func (s *PGSqlxSuite) SetupSuite() {
	s.PsqlSqlxParallelSuite.SetupSuite()

	s.repo = NewPGSqlxRepository(logger.NewMockLogger(), s.TestPsqlClient)
}

func (s *PGSqlxSuite) TearDownTest() {
	s.PsqlSqlxParallelSuite.TearDownTest()
}

func (s *PGSqlxSuite) TestCreate() {
	ctx, tx := s.CtxTx()

	_order := models.Order{Title: "title"}
	order, err := s.repo.Create(ctx, tx, _order)
	s.Require().NoError(err)

	order.Id = _order.Id
	s.Equal(_order, order)
}

func (s *PGSqlxSuite) TestGet() {
	ctx, tx := s.CtxTx()

	data := models.Order{Title: "title"}
	_order, err := s.repo.Create(ctx, tx, data)
	s.Require().NoError(err)

	order, err := s.repo.GetById(ctx, tx, _order.Id)
	s.Require().NoError(err)

	s.Equal(_order, order)
}
