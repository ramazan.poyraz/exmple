package orders

import (
	"context"

	"base/internal/repository/models"
	"base/internal/repository/transaction"
)

//go:generate  mockgen -source interface.go -destination ../../mocks/repository/admin/interface.go

type Repository interface {
	GetById(ctx context.Context, tx transaction.Tx, id int64) (models.Order, error)
	Create(ctx context.Context, tx transaction.Tx, data models.Order) (models.Order, error)
	CreateNonTx(ctx context.Context, data models.Order) (models.Order, error)
}
