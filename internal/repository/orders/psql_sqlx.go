package orders

import (
	"context"
	"database/sql"
	"errors"

	"base/internal/db"
	errors2 "base/internal/errors"
	"base/internal/logger"
	"base/internal/repository/models"
	"base/internal/repository/transaction"
)

type repositorySqlxPG struct {
	log logger.Logger

	pgClient *db.PostgresClient
}

func NewPGSqlxRepository(log logger.Logger, pgClient *db.PostgresClient) Repository {
	return &repositorySqlxPG{
		log:      log.Named("pg_order_repo"),
		pgClient: pgClient,
	}
}

func (r *repositorySqlxPG) GetById(ctx context.Context, tx transaction.Tx, id int64) (data models.Order, err error) {
	query := `SELECT id, title FROM orders WHERE id=$1`
	err = tx.Txm().QueryRowxContext(ctx, query, id).StructScan(&data)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return data, err
		}
		return data, r.log.ErrorRepo(err, errors2.PsqlRequest, query)
	}

	return data, nil
}

func (r *repositorySqlxPG) Create(ctx context.Context, tx transaction.Tx, data models.Order) (models.Order, error) {
	query := `INSERT INTO orders (title) VALUES ($1) RETURNING id`
	err := tx.Txm().QueryRowxContext(ctx, query, data.Title).Scan(&data.Id)
	if err != nil {
		return data, r.log.ErrorRepo(err, errors2.PsqlRequest, query)
	}

	return data, nil
}

func (r *repositorySqlxPG) CreateNonTx(ctx context.Context, data models.Order) (models.Order, error) {
	query := `INSERT INTO orders (title) VALUES ($1) RETURNING id`
	err := r.pgClient.DBSqlx.QueryRowxContext(ctx, query, data.Title).Scan(&data.Id)
	if err != nil {
		return data, r.log.ErrorRepo(err, errors2.PsqlRequest, query)
	}

	return data, nil
}
