package domain

type Order struct {
	Id    int64
	Title string
}
