package connections

import (
	amqp "github.com/rabbitmq/amqp091-go"
)

type (
	Rabbitmq struct {
		Conn *amqp.Connection
	}
)

func NewRabbitMQConn() (*Rabbitmq, error) {
	c, err := amqp.Dial("amqp://admin:admin@localhost:5672/")
	if err != nil {
		return nil, err
	}

	return &Rabbitmq{
		Conn: c,
	}, nil
}
