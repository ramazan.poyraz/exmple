package errors

import (
	"net/http"
)

const (
	ROW_NOT_FOUND = "row not found"
)

var (
	TxError = &Error{Code: http.StatusInternalServerError, Reason: "tx error"}

	PsqlRequest = New("postgresql request error")
)

func DatabaseError(details error) *Error {
	return WD(&Error{Code: http.StatusInternalServerError, Reason: "database failed"}, details)
}
