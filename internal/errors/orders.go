package errors

import (
	"fmt"
	"net/http"
)

func OrderNotFound(id int64) *Error {
	return &Error{
		Code:    http.StatusBadRequest,
		Reason:  ROW_NOT_FOUND,
		Details: New(fmt.Sprintf("order with id %d not found", id)),
	}
}
