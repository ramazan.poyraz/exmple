package errors

import (
	"errors"
	"fmt"
)

func New(text string) error {
	return errors.New(text)
}

func Is(err, target error) bool {
	return errors.Is(err, target)
}

// WD - with details
func WD(err *Error, details error) *Error {
	e := *err
	e.Details = details
	return &e
}

func WDF(err1 *Error, format string, params ...any) *Error {
	e := *err1
	e.Details = fmt.Errorf("%s: %s", err1.Details, fmt.Sprintf(format, params...))
	return &e
}

// WDString - with text details
func WDString(err *Error, details string) *Error {
	e := *err
	e.Details = New(details)
	return &e
}
