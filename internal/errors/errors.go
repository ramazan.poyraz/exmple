package errors

import (
	"fmt"
	"net/http"
)

type Error struct {
	Code    int    `json:"code"`
	Reason  string `json:"reason"`
	Details error  `json:"details"`
}

func (e *Error) Error() string {
	return fmt.Sprintf("%d: %s | %s", e.Code, e.Reason, e.Details)
}

var (
	PermissionError    = &Error{Code: http.StatusForbidden, Reason: "permission denied"}
	ParseError         = &Error{Code: http.StatusBadRequest, Reason: "parse error"}
	InternalError      = &Error{Code: http.StatusInternalServerError, Reason: "internal server error"}
	MissingCredentials = &Error{Code: http.StatusUnauthorized, Reason: "missing credentials"}
	ValidationError    = &Error{Code: http.StatusUnprocessableEntity, Reason: "validation error"}
	TimeoutError       = &Error{Code: http.StatusRequestTimeout, Reason: "operation timed out"}
	InvalidVersion     = &Error{Code: http.StatusUnprocessableEntity, Reason: "invalid version"}
)
