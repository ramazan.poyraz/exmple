package errors

import (
	"net/http"
)

var (
	NoQueryParameterError = &Error{Code: http.StatusBadRequest, Reason: "no query parameter specified"}
)
