package main

import (
	"flag"

	"base/internal/app"
)

func main() {
	var path, env string
	flag.StringVar(&path, "path", "", "config file path")
	flag.StringVar(&env, "env", "", "config file path")
	flag.Parse()

	application := app.NewApplication(path, env)
	application.RunRabbitmq()
}
