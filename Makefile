postgres.start:
	if [ ! "$(shell docker ps -q -f name=test-postgres)" ]; then \
        if [ "$(shell docker ps -aq -f status=exited -f name=test-postgres)" ]; then \
            docker rm test-postgres; \
        fi; \
		docker run --restart unless-stopped -d -p 1338:5432 -e POSTGRES_PASSWORD=1337 -e POSTGRES_USER=admin --name test-postgres postgres:15 ;\
        sleep 5s; \
    fi;
	-docker exec test-postgres psql -U admin -c "create database main"
	-docker exec test-postgres psql -U admin -c "grant all privileges on database main to admin"
postgres.stop:
	docker stop test-postgres
	docker rm test-postgres

migrate.up:
	goose -dir migrations -allow-missing postgres "host=localhost port=1338 user=admin password=1337 dbname=main sslmode=disable" up

migrate.down:
	goose -dir migrations -allow-missing postgres "host=localhost port=1338 user=admin password=1337 dbname=main sslmode=disable" down

openapi.generate:
	oapi-codegen --package=models --generate types ./specs/api.yaml > ./internal/generated/models/openapi.gen.go
